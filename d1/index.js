const express = require('express');

//Created an application using express
//In layman's term, app is our server
const app = express();

const port = 4000;

//Middleware is a software that provides common services and capabilities to applications outside of what's offerec by the operating system
//use() => is a method to configure the middleware used by the routes of express
//express.json() => allows your app to read json data
app.use(express.json());
//By default, information received from the url can only be received as a string or an array.
//By applying the "extended:true" inside the express.urlencoded, this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({ extended:true }));

//Mock database
let users = []

//Routes/endpoint
//http://localhost:4000/
//get
app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!")
});

//post

app.post("/hello", (req, res) => {
	//req.body contains the contents/data of the request body
	//
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

//Create a POST route to register a user.

app.post("/signup", (req, res) => {
	console.log(req.body);
	//validation
	//If contents of the req.body with the property username and password is not empty, then push the data to the users array. else, please input both username and password
	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`)
	} else{
		res.send("Please input BOTH username and password")
	}
})


//This routes expects to receive a PUT request at the URI "/change-password"
//put
//This will update the password or a user that matches the information provided in the client
app.put("/change-password", (req, res) => {

let message;

	for(let i = 0; i < users.length; i++){
		//if the username provided in the postman/client and the username of the current object in the loop is the same.
		if(req.body.username === users[i].username){
			//change the password of the user found by the loop into the provided in the client
			users[i].password = req.body.password

			//message response 
			message = `User ${req.body.username}'s password has been updated`;

			//Breaks out of the loop once a user matches the username provided.
			break;
		}else {
			message = "User does not exist"
		}
	}

	res.send(message);

})

//1. Create a GET route that will access the "/home" route that will print out a simple message.
app.get("/home", (req, res) => {
	res.send("Welcome to the Home Page")
});


// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
app.get("/users",(req, res) => {
    res.send(users)
})

// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete("/delete-user", (req, res) => {
	let delUser;
	for(let i=0; i<users.length; i++)
	if(users[i].username === req.body.username) {
		delUser = `delete ${req.body.username}`
		break;
	} else {
		delUser = `unknown username`
	}
	res.send(delUser);
})


//returns a message to confirm that server is running in the terminal
app.listen(port, ()=> console.log(`Server running at port:${port}`));



